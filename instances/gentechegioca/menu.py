#!/usr/bin/env python
# encoding: utf-8
def config(root,application=None):

    auto = root.branch(u"RPG DB")
    
    control_panel = auto.branch(u"Control Panel", tags="admin,manager")
    control_panel.thpage(u"!!Auth Tags", table="adm.htag")
    control_panel.thpage(u"!!Errors", table="sys.error")
    control_panel.thpage(u"!!Utenti", table="adm.user")

    gcg = auto.branch(u"Archivi principali", tags="staff")
    gcg.thpage(u"!!Giocatore", table="gcg.giocatore")
    gcg.thpage(u"!!Gioco", table="gcg.gioco")
    gcg.thpage(u"!!Tipo di gioco", table="gcg.tipo_gioco")
    gcg.lookups(u"Tabelle di servizio", lookup_manager="gcg")

    #auto.thpage(u"!!Tipo dado", table="gcg.meccanica")
    #auto.thpage(u"!!Lv_esperienza", table="gcg.lv_esperienza")
    

