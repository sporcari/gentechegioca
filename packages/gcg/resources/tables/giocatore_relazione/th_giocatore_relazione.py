#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('giocatore_id')
        r.fieldcell('relazione_id')
        r.fieldcell('tipo_relazione')
        r.fieldcell('giochi_insieme_pkeys')

    def th_order(self):
        return 'giocatore_id'

    def th_query(self):
        return dict(column='giocatore_id', op='contains', val='%')
        
class ViewFromGiocatore(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('@relazione_id', name='Giocatore', width='20em', edit=True)
        r.fieldcell('@relazione_id.provincia', name='Provincia', width='10em')
        r.fieldcell('@relazione_id.@user_id.email', name='Email', width='10em')
        r.fieldcell('tipo_relazione', width='10em', name='Tipo relazione',edit=True)
        #r.fieldcell('giochi_insieme_pkeys', width='10em', name='Giocati insieme')

    def th_order(self):
        return 'relazione_id'


class ViewFromRelazione(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('@giocatore_id.username', name='Giocatore', width='20em')
        r.fieldcell('@giocatore_id.provincia', name='Provincia', width='10em')
        r.fieldcell('@giocatore_id.@user_id.email', name='Email', width='10em')
        r.fieldcell('esperienza_id', width='15em', name='Esperienza')

    def th_order(self):
        return '@esperienza_id._row_count'


class Form(BaseComponent):

    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing='4px')
        fb.field('candidato_id')
        fb.field('lingua_id')


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
