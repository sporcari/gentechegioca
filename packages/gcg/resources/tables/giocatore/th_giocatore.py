#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('username')
        r.fieldcell('nome')
        r.fieldcell('cognome')
        r.fieldcell('eta', width='5em')
        r.fieldcell('data_nascita')
        r.fieldcell('provincia', width='5em')
        r.fieldcell('sesso', width='3em')
        r.fieldcell('giochi_conosciuti', width='20em')

    def th_order(self):
        return 'username'

    def th_query(self):
        return dict(column='username', op='contains', val='', )


class Form(BaseComponent):

    def th_form(self, form):
        bc = form.center.borderContainer()
        top = bc.borderContainer(region='top', height='250px', datapath='.record')
        tc = bc.tabContainer(region='center')
        self.giochiGrid(tc.contentPane(title='Giochi'))
        self.linksGrid(tc.contentPane(title='Links'))
        self.relazioniBc(tc.borderContainer(title='Relazioni'))

        top.roundedGroup(title='Avatar', region='left',width='200px').img(src='^.avatar_url',crop_width='173px',crop_height='210px',
                        placeholder=self.getResourceUri('images/missing_photo.png'),
                        upload_folder='vol:doc/giocatori/avatar',edit=True,
                        upload_filename='=#FORM.record.id',crop_border='1px solid #ddd',crop_rounded=8,crop_margin='5px',
                       rowspan=5)


        utentePane = top.roundedGroup(region='center', title='Dati personali')
        fb = utentePane.div(margin_right='15px').formbuilder(cols=2, border_spacing='4px', width='100%', fld_width='100%')
        fb.field('user_id', lbl='Username', unmodifiable=True)
        fb.field('sesso', tag='filteringSelect', values='M,F')
        fb.field('@user_id.email', tag='div',lbl='Email Reg.', colspan=2)
        fb.field('@user_id.firstname', lbl='Nome')
        fb.field('@user_id.lastname', lbl='Cognome')
        fb.field('data_nascita')
        fb.field('anno_esordio', lbl='Gioca dal')
        fb.field('professione', lbl='Professione')
        fb.field('skype', lbl='Skype')
        fb.field('facebook', lbl='Facebook')
        fb.field('twitter', lbl='Twitter')
        fb.field('gplus', lbl='G+')

        geoPane = top.roundedGroup(region='right', title='Dati geografici', width='500px')
        fb = geoPane.div(margin_right='15px').formbuilder(cols=3, border_spacing='4px', width='100%', fld_width='100%')
        fb.geoCoderField(value='^.$cerca_luogo',
                 colspan=3,
                 selected_locality='.localita',
                 selected_postal_code='.cap',
                 selected_administrative_area_level_2 ='.provincia',
                 selected_country='.nazione',
                 ghost='Località',
                 lbl='Cerca Luogo')
        fb.field('localita', colspan=3)
        fb.field('cap')
        fb.field('provincia')
        fb.field('nazione')

    def linksGrid(self, pane):
        pane.inlineTableHandler(relation='@links',
                                        viewResource='ViewFromGiocatore',
                                        addrow=True,
                                        delrow=True)
    def giochiGrid(self, pane):
        pane.inlineTableHandler(relation='@giochi',
                                        viewResource='ViewFromGiocatore',
                                        picker='gioco_id',
                                        addrow=True,
                                        delrow=True)

    def relazioniBc(self, bc):
        lpane = bc.contentPane(region='left', width='50%')
        rpane = bc.contentPane(region='center')
        lpane.inlineTableHandler(relation='@relazioni_mie',
                                        viewResource='ViewFromGiocatore',
                                        picker='relazione_id',
                                        picker_condition = '$id!=:giocatore_id',
                                        picker_condition_giocatore_id='^#FORM.pkey',
                                        addrow=True,
                                        delrow=True, nodeId='relazioni_mie')

        rpane.plainTableHandler(relation='@relazionato_da',
                                        viewResource='ViewFromRelazione',
                                        nodeId='relazionato_da')

    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
