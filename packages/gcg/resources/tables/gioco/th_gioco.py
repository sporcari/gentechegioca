#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('sigla', width='5em')
        r.fieldcell('titolo', width='20em')
        r.fieldcell('autore', width='15em')
        r.fieldcell('tipo_gioco_id', width='15em')
        r.fieldcell('ambientazione', width='15em')
        r.fieldcell('genere', width='15em')
        r.fieldcell('meccanica', width='15em')
        r.fieldcell('lingue', width='10em')
        r.fieldcell('pagine', width='5em')
        r.fieldcell('anno_pubblicazione', width='5em')
        r.fieldcell('ng_min')
        r.fieldcell('ng_max')
        r.fieldcell('gm', name='GM')

    def th_order(self):
        return 'titolo'

    def th_query(self):
        return dict(column='titolo', op='contains', val='')

    def th_options(self):
        return dict(virtualStore=False)


class ViewFromTipo(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('sigla', width='5em')
        r.fieldcell('titolo', width='25em')
        r.fieldcell('autore', width='25em')
        r.fieldcell('ambientazione', width='15em')
        r.fieldcell('genere', width='15em')
        r.fieldcell('meccanica', width='15em')
        r.fieldcell('lingue', width='10em')
        r.fieldcell('pagine', width='5em')
        r.fieldcell('anno_pubblicazione', width='5em')
        r.fieldcell('ng_min')
        r.fieldcell('ng_max')
        r.fieldcell('gm', name='GM')

    def th_order(self):
        return 'titolo'

    def th_query(self):
        return dict(column='titolo', op='contains', val='')

    def th_options(self):
        return dict(virtualStore=False)

class Form(BaseComponent):
    py_requires="""gnrcomponents/dynamicform/dynamicform:DynamicForm,
                    gnrcomponents/attachmanager/attachmanager:AttachManager"""

    def th_form(self, form):
        bc = form.center.borderContainer()
        self.topPanes(bc.borderContainer(region='top', height='280px', datapath='.record'))
        tc = bc.tabContainer(region='center')
        self.linksGrid(tc.contentPane(title='Links'))
        self.atcGrid(tc.contentPane(title='Documenti'))
        self.giocatoriGrid(tc.contentPane(title='Giocatori'))

    def topPanes(self, top):
        self.schedaGioco(top.roundedGroup(region='center', title='Informazioni di base'))
        top_right= top.roundedGroup(region='right', title='Dettagli', width='400px')
        top_right.dynamicFieldsPane(field='caratteristiche')

    def schedaGioco(self, pane):
        fb = pane.div(margin_right='30px').formbuilder(cols=4, border_spacing='5px', width='100%', fld_width='100%')
        fb.field('titolo', colspan=2)
        fb.field('autore', colspan=2, lbl='Autori', tag='dbCombobox',
                  dbtable='gcg.autore',columns='nome')
        fb.field('sigla', width='5em')
        fb.field('tipo_gioco_id',lbl='Tipo', tag='hdbSelect', colspan=3)
        fb.field('pagine', lbl='Pagine', width='5em')
        fb.field('anno_pubblicazione', width='5em')
        fb.field('lingue', tag='checkBoxText', values='ITA,ENG,FR,ESP', popup=True, colspan=2)

        fb.field('meccanica_id',lbl='Meccanica', hasDownArrow=True, colspan=2)
        fb.field('genere_id',lbl='Genere', hasDownArrow=True, colspan=2)
        

        d = fb.div(lbl='Giocatori (Da/A)', colspan=2)
        d.field('ng_min', width='3em')
        d.field('ng_max', width='3em')
        fb.field('gm', lbl='', label='Con GM', colspan=2)
        fb.field('descrizione', tag='simpleTextArea', height='130px', colspan=4, lbl_pos='T')

    def atcGrid(self, pane):

        pane.attachmentPane(mode='sidebar',pbl_classes='*',
                                viewResource='gnrcomponents/attachmanager/attachmanager:AttachManagerViewBase')

    def linksGrid(self, pane):
        pane.inlineTableHandler(relation='@links',
                                        viewResource='ViewFromGioco',
                                        addrow=True,
                                        delrow=True)

    def giocatoriGrid(self, pane):
        pane.plainTableHandler(relation='@giocatori',
                                viewResource='ViewFromGioco')
class FormFromTipo(Form):

    def scheda_gioco(self, pane):
        fb = pane.div(margin_right='30px').formbuilder(cols=4, border_spacing='5px', width='100%', fld_width='100%')
        fb.field('titolo', colspan=2)
        fb.field('autore', lbl='Autori', tag='dbCombobox',
                  dbtable='gcg.autore',columns='nome')
        fb.field('pagine', lbl='Pagine', width='5em')
        fb.field('anno_pubblicazione', width='5em')
        fb.field('lingue', tag='checkBoxText', values='ITA,ENG,FR,ESP', popup=True, colspan=2)

        fb.field('sigla', width='5em')
        fb.field('meccanica_id',lbl='Meccanica', hasDownArrow=True, colspan=2)
        fb.field('genere_id',lbl='Genere', hasDownArrow=True, colspan=2)
        

        d = fb.div(lbl='Giocatori (Da/A)', colspan=2)
        d.field('ng_min', width='3em')
        d.field('ng_max', width='3em')
        fb.field('gm', lbl='', label='Con GM', colspan=2)
        fb.field('descrizione', tag='simpleTextArea', height='130px', colspan=4, lbl_pos='T')

    def th_options(self):
        return dict(dialog_height='300px', dialog_width='1000px')
