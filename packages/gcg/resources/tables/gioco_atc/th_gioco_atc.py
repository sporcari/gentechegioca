#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent

class ViewFromGioco(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('description', name='Titolo', width='100%')
        r.fieldcell('fileurl',hidden=True)
        r.cell('imp',calculated=True,name='!!Imp.',format_isbutton=True,format_buttonclass='iconbox document',
                format_onclick="""
                    genro.serverCall('_table.'+this.attr.table+'.atc_importAttachment',{pkey:this.widget.rowIdByIndex($1.rowIndex)},
                                     function(){console.log("ocr done")});
                """,width='22px')