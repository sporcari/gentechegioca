#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('giocatore_id')
        r.fieldcell('gioco_id')

    def th_order(self):
        return 'gioco_id'

    def th_query(self):
        return dict(column='gioco_id', op='contains', val='%')
        
class ViewFromGioco(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('@giocatore_id.username', name='Giocatore', width='20em')
        r.fieldcell('@giocatore_id.provincia', name='Provincia', width='10em')
        r.fieldcell('@giocatore_id.@user_id.email', name='Email', width='10em')
        r.fieldcell('esperienza_id', width='10em', name='Esperienza')

    def th_order(self):
        return 'giocatore_id'


class ViewFromGiocatore(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('gioco_id', name='Gioco', width='100%', edit=True)
        r.fieldcell('esperienza_id', width='15em', name='Esperienza',  edit=True)

    def th_order(self):
        return '@esperienza_id._row_count'


class Form(BaseComponent):

    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing='4px')
        fb.field('candidato_id')
        fb.field('lingua_id')


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
