#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('gioco_id')
        r.fieldcell('descrizione')
        r.fieldcell('url')

    def th_order(self):
        return '_row_count'

    def th_query(self):
        return dict(column='descrizione', op='contains', val='')

class ViewFromGiocatore(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('url', edit=True, width='35em')
        r.fieldcell('descrizione', edit=True, width='100%')
        

    def th_order(self):
        return '_row_count'

    def th_query(self):
        return dict(column='descrizione', op='contains', val='')


class Form(BaseComponent):

    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing='4px')
        fb.field('gioco_id')
        fb.field('descrizione')
        fb.field('url')


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
