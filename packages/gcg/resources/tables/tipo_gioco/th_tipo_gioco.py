from gnr.web.gnrbaseclasses import BaseComponent

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('hierarchical_descrizione')

    def th_order(self):
        return 'hierarchical_descrizione'

    def th_query(self):
        return dict(column='hierarchical_descrizione', op='contains', val='')



class Form(BaseComponent):
    py_requires = 'gnrcomponents/dynamicform/dynamicform:DynamicForm'
    def th_form(self, form):
        bc = form.center.borderContainer()
        fb = bc.contentPane(region='top',datapath='.record').formbuilder(cols=2, border_spacing='4px')
        fb.field('descrizione',validate_notnull=True, width='30em')
        tc = bc.tabContainer(region='center')
        tc.contentPane(title='Giochi').dialogTableHandler(relation='@giochi', formResource='FormFromTipo')
        tc.contentPane(title='Campi aggiuntivi').fieldsGrid(margin='2px',rounded=6,border='1px solid silver')


    def th_options(self):
        return dict(hierarchical=True)

