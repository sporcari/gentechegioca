#!/usr/bin/env python
# encoding: utf-8
def config(root,application=None):
    auto = root.branch(u"RPG DB")
    auto.thpage(u"!!Giocatore", table="gcg.giocatore")
    auto.thpage(u"!!Gioco", table="gcg.gioco")
    auto.thpage(u"!!Tipo di gioco", table="gcg.tipo_gioco")
    auto.lookups(u"Tabelle di servizio", lookup_manager="gcg")