#!/usr/bin/env python
# encoding: utf-8

class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table('tipo_gioco', pkey='id', name_long='!!Tipo gioco',
                         name_plural='!!Tipo gioco',
                        caption_field='hierarchical_descrizione')
        self.sysFields(tbl,hierarchical='descrizione',counter=True, df=True)
        #tbl.column('sigla' ,name_long='!!Sigla')
        tbl.column('descrizione', name_long='!!Descrizione')