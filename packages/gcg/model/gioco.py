#!/usr/bin/env python
# encoding: utf-8

class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table('gioco', pkey='id', name_long='!!Gioco', name_plural='!!Giochi',caption_field='titolo')
        self.sysFields(tbl)
        tbl.column('tipo_gioco_id',
                   size='22' ,group='_',
                   name_long='!!Tipo Gioco',
                   name_short='Tipo').relation('tipo_gioco.id',
                                                relation_name='giochi',
                                                mode='foreignkey',
                                                onDelete='raise')
        tbl.column('sigla' ,name_long='!!Sigla')
        tbl.column('titolo',name_long='!!Titolo')
        tbl.column('descrizione', name_long='!!Descrizione')
        tbl.column('autore', name_long='!!Autore')
        tbl.column('pagine', dtype='N', name_long='!!N.Pagine')
        tbl.column('anno_pubblicazione', name_long='!!Anno')
        tbl.column('lingue', name_long='!!Lingue')
        tbl.column('gm', dtype='B', name_long='!!Con GM')
        tbl.column('ng_min', dtype='N', name_long='N.Min Giocatori')
        tbl.column('ng_max', dtype='N', name_long='N.Max Giocatori')
        tbl.column('genere_id',
                   size='22' ,group='_',
                   name_long='!!Genere id',
                   name_short='Genere id').relation('genere.id',
                                                relation_name='giochi',
                                                mode='foreignkey',
                                                onDelete='raise')
        tbl.column('ambientazione_id',
                   size='22' ,group='_',
                   name_long='!!Ambientazione id',
                   name_short='Genere id').relation('ambientazione.id',
                                                relation_name='giochi',
                                                mode='foreignkey',
                                                onDelete='raise')
        tbl.column('meccanica_id', name_long='!!Meccanica id', group='_').relation('gcg.meccanica.id',
                                                                      onDelete='raise',
                                                                       mode='foreignkey',
                                                                       one_group='_',
                                                                       many_group='_',
                                                                      relation_name='giochi')
        tbl.column('img_url', dtype='P',name_long='!!Copertina',name_short='Copertina')
        tbl.column('caratteristiche',dtype='X',name_long='!!Dettagli',subfields='tipo_gioco_id')
        tbl.aliasColumn('genere', relation_path='@genere_id.nome', name_long='Genere')
        tbl.aliasColumn('ambientazione', relation_path='@ambientazione_id.nome', name_long='Ambientazione')
        tbl.aliasColumn('meccanica', relation_path='@meccanica_id.nome', name_long='Meccanica')
        #tbl.column('conti_pkeys',name_long='!!Conti pkeys',caption_field='conti_caption')
        #tbl.formulaColumn('conti_caption',"array_to_string(ARRAY(#conti_desc),',')",
        #              select_conti_desc=dict(table='sw_coge.conto',columns='$nome',
        #              where="$id = ANY(string_to_array(#THIS.conti_pkeys,','))",order_by='$_row_count'))#