#!/usr/bin/env python
# encoding: utf-8

class Table(object):
    """ """
    def config_db(self, pkg):
        tbl =  pkg.table('gioco_link',  pkey='id',name_plural = '!!Links',
                         name_long=u'!!Link')
        self.sysFields(tbl, counter=True)
        tbl.column('giocatore_id',size='22',name_long = '!!Giocatore',group='_').relation('gcg.giocatore.id',
                                                                                   onDelete='raise',
                                                                                    one_group='_',
                                                                                     many_group='_',
                                                                                    mode='foreignkey',
                                                                                   relation_name='links')
        tbl.column('descrizione',name_long = '!!Descrizione')
        tbl.column('url',name_long = '!!URL')