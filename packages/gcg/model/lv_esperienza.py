#!/usr/bin/env python
# encoding: utf-8

class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table('lv_esperienza', pkey='id', name_long='!!Esperienza di gioco', 
                        name_plural='!!Livelli esperienza',caption_field='descrizione',lookup=True, order_by='_row_count')
        self.sysFields(tbl, counter=True)
        tbl.column('descrizione',name_long='!!Descrizione')