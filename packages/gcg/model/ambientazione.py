#!/usr/bin/env python
# encoding: utf-8

class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table('ambientazione', pkey='id', name_long='!!Ambientazione', 
                        name_plural='!!Ambientazioni',caption_field='nome',lookup=True)
        self.sysFields(tbl)
        tbl.column('nome',name_long='!!Nome')
        tbl.column('descrizione',name_long='!!Descrizione estesa')