#!/usr/bin/env python
# encoding: utf-8
# VISTOAL: 291008
class Table(object):
    """ """
    def config_db(self, pkg):
        
        tbl =  pkg.table('giocatore',  pkey='id',name_plural = '!!Giocatori',
                         name_long=u'!!Giocatore', rowcaption='', caption_field='username', 
                          group_da='Personali',
                          group_cnt='Contatti',
                          group_geo='Geografici')
        self.sysFields(tbl)
        tbl.column('user_id' ,name_long='!!Utente',name_short='utente').relation('adm.user.id',  onDelete='cascade',
                                                                                   deferred= True,
                                                                                    mode='foreignkey',
                                                                                    one_group='_',
                                                                                    many_group='_',
                                                                                   relation_name='giocatore')
        tbl.column('cap', group='geo',cname_long ='!!Cap')
        tbl.column('localita', group='geo', name_long ='!!Localita')
        tbl.column('provincia',group='geo', name_long ='!!Provincia',indexed=True).relation('glbl.provincia.sigla',mode='foreignkey', one_group='_', many_group='_')
        tbl.column('nazione',size='2',group='geo',name_long='!!Nazione').relation('glbl.nazione.code',mode='foreignkey', one_group='_', many_group='_')
        tbl.column('sesso', group='da', name_long ='!!Sesso')
        tbl.column('data_nascita', dtype ='D', name_long ='!! Data nascita', group='da')
        tbl.column('skype',name_long='!!Skype', group='cnt')
        tbl.column('facebook',name_long='!!Facebook', group='cnt')
        tbl.column('gplus',name_long='!!G+', group='cnt')
        tbl.column('twitter',name_long='!!Twitter', group='cnt')
        tbl.column('professione', name_long='Professione', group='da')
        tbl.column('anno_esordio', name_long='Giocatore dal', group='da')
        tbl.column('avatar_url', dtype='P',name_long='!!Avatar URL', group='_')
        tbl.formulaColumn('eta','extract(YEAR FROM age($data_nascita))',dtype='L',group='da',name_long=u'!!Età')

        tbl.aliasColumn('username', '@user_id.username', name_long='Username')
        tbl.aliasColumn('nome', '@user_id.firstname', name_long='Nome')
        tbl.aliasColumn('cognome', '@user_id.lastname', name_long='Cognome')
        tbl.aliasColumn('email','@user_id.email', name_long='email', group='cnt')
        tbl.aliasColumn('giochi_conosciuti','@giochi.gioco_esperienza', aggregator='<br/>' ,name_long='Giochi conosciuti')
