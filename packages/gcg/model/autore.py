#!/usr/bin/env python
# encoding: utf-8

class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table('autore', pkey='id', name_long='!!Autore',
                         name_plural='!!Autori',
                        caption_field='nome', lookup=True)
        self.sysFields(tbl)
        #tbl.column('sigla' ,name_long='!!Sigla')
        tbl.column('nome', name_long='!!Nome')
        tbl.column('sito', name_long='!!Sito')
