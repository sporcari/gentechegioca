#!/usr/bin/env python
# encoding: utf-8
# VISTOAL: 291008
class Table(object):
    """ """
    def config_db(self, pkg):
        tbl =  pkg.table('gioco_giocatore',  pkey='id',name_plural = '!!Giochi Giocatore',
                         name_long=u'!!Giochi Giocatori')
        self.sysFields(tbl)
        tbl.column('giocatore_id',size='22',name_long = '!!Giocatore',group='_').relation('gcg.giocatore.id',
                                                                                   onDelete='cascade',
                                                                                   deferred= True,
                                                                                    mode='foreignkey',
                                                                                   one_group='_',
                                                                                    many_group='_',
                                                                                   relation_name='giochi')
        tbl.column('gioco_id',size='22',name_long = '!!Gioco id',group='_').relation('gcg.gioco.id',
                                                                                   onDelete='raise',
                                                                                   deferred= True,
                                                                                   # one_group='_',
                                                                                    # many_group='_',
                                                                                    mode='foreignkey',
                                                                                   relation_name='giocatori')
        tbl.column('esperienza_id', size='22',name_long = '!!Esperienza id',group='_').relation('gcg.lv_esperienza.id',
                                                                                   onDelete='raise',
                                                                                   deferred= True,
                                                                                    one_group='_',
                                                                                     many_group='_',
                                                                                    mode='foreignkey',
                                                                                   relation_name='giochi_giocatori')
        tbl.aliasColumn('gioco', '@gioco_id.titolo', name_long='Gioco')
        tbl.aliasColumn('giocatore', '@giocatore_id.username', name_long='Giocatore')
        tbl.aliasColumn('esperienza', '@esperienza_id.descrizione', name_long='Esp')
        tbl.formulaColumn('gioco_esperienza', "$gioco||' - '||$esperienza", name_long='Gioco/Esperienza')
        tbl.formulaColumn('giocatore_esperienza', "$giocatore||'-'||$esperienza", name_long='Giocatore/Esperienza')