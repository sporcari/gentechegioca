#!/usr/bin/env python
# encoding: utf-8
# VISTOAL: 291008
class Table(object):
    """ """
    def config_db(self, pkg):
        tbl =  pkg.table('gioco_relazione',  pkey='id',name_plural = '!!Relazioni',
                         name_long=u'!!Relazione Giocatori')
        self.sysFields(tbl)
        tbl.column('giocatore_id',size='22',name_long = '!!Giocatore',group='_').relation('gcg.giocatore.id',
                                                                                   onDelete='cascade',
                                                                                   deferred= True,
                                                                                    mode='foreignkey',
                                                                                    many_name='Relazioni',
                                                                                    one_name='Aggiunto da',
                                                                                    one_group='_',
                                                                                    many_group='_',
                                                                                   relation_name='relazionato_da')
        tbl.column('relazione_id',size='22',name_long = '!!Relazione',group='_').relation('gcg.giocatore.id',
                                                                                   onDelete='raise',
                                                                                    many_name='Aggiunto da',
                                                                                    one_name='Relazioni',
                                                                                   deferred= True,
                                                                                    one_group='_',
                                                                                     many_group='_',
                                                                                    mode='foreignkey',
                                                                                   relation_name='relazioni_mie')
        tbl.column('tipo_relazione', name_long='!!Tipo Relazione')
        tbl.column('giochi_insieme_pkeys', name_long='!!Giochi')