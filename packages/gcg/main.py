#!/usr/bin/env python
# encoding: utf-8
from gnr.app.gnrdbo import GnrDboTable, GnrDboPackage

class Package(GnrDboPackage):
    def config_attributes(self):
        return dict(comment='gcg package',sqlschema='gcg',
                    name_short='Gcg', name_long='Gcg', name_full='Gcg')
                    
    def config_db(self, pkg):
        pass
        
class Table(GnrDboTable):
    pass
